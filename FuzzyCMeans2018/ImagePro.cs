﻿
namespace FuzzyCMeans2018
{
    class ImagePro
    {
        //------------------------------------------------------------------------
        // Name             : ShellSort
        // Function         : 並び替え
        // Argument			: 数値データ
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public double[] ShellSort(double[] SortFile)
        {
            int len = SortFile.Length;
            int inter = 0;

            while (inter <= len / 9)
            {
                inter = 3 * inter + 1;
            }

            for (; inter > 0; inter /= 3)
            {
                for (int ii = inter; ii < len; ii++)
                {
                    double tmp = SortFile[ii];
                    if (SortFile[ii - inter].CompareTo(tmp) > 0)
                    {
                        int jj = ii;
                        do
                        {
                            SortFile[jj] = SortFile[jj - inter];
                            jj = jj - inter;
                        } while (jj >= inter && SortFile[jj - inter].CompareTo(tmp) > 0);

                        SortFile[jj] = tmp;
                    }
                }
            }
            return SortFile;
        }


        //------------------------------------------------------------------------
        // Name             : OutArray
        // Function         : 1次元配列をマスク領域に出力
        // Argument			: 入力データ，マスクデータ
        // Return			: マスク領域
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: マスク領域を1次元で返す．
        //------------------------------------------------------------------------
        public void OutArray(FileData output, FileData mask, double[] val)
        {
            int ii = 0;
            int sam = output.OutSamples();
            int lin = output.OutLines();

            double[,] val_out = new double[lin, sam];
            double[,] val_mask = mask.OutVal();

            for (int yy = 0; yy < lin; yy++)
            {
                for (int xx = 0; xx < sam; xx++)
                {
                    if (val_mask[yy, xx] != 0)
                    {
                        val_out[yy, xx] = val[ii];
                        ii++;
                    }
                    else { }
                }
            }

            output.InVal(val_out);
        }


        //------------------------------------------------------------------------
        // Name             : GetArray
        // Function         : マスク領域を1次元配列に入力
        // Argument			: 入力データ，マスクデータ
        // Return			: マスク領域
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: マスク領域を1次元で返す．
        //------------------------------------------------------------------------
        public double[] InArray(FileData input, FileData mask)
        {
            double[] val = new double[AreaCount(mask)];

            int ii=0;
            int sam = input.OutSamples();
            int lin = input.OutLines();

            double[,] val_in = input.OutVal();
            double[,] val_mask = mask.OutVal();

            for (int yy = 0; yy < lin; yy++)
            {
                for (int xx = 0; xx < sam; xx++)
                {
                    if (val_mask[yy, xx] != 0)
                    {
                        val[ii] = val_in[yy, xx];
                        ii++;
                    }
                    else { }
                }
            }
            return val;
        }


        //------------------------------------------------------------------------
        // Name             : AreaCount
        // Function         : マスク処理
        // Argument			: マスクデータ
        // Return			: マスク領域数
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: マスク領域数をカウントする．
        //------------------------------------------------------------------------
        public int AreaCount(FileData mask)
        {
            int sam = mask.OutSamples();
            int lin = mask.OutLines();

            int cnt = 0;

            double[,] val_mask = mask.OutVal();

            for (int yy = 0; yy < lin; yy++)
            {
                for (int xx = 0; xx < sam; xx++)
                {
                    if (val_mask[yy, xx] != 0)
                    {
                        cnt++;
                    }
                    else { }
                }
            }
            return cnt;
        }


        //------------------------------------------------------------------------
        // Name             : Masking
        // Function         : マスク処理
        // Argument			: 入力データ，出力データ，マスクデータ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: "1"の箇所をマスクする．
        //------------------------------------------------------------------------
        public void Masking(FileData input, FileData output, FileData mask)
        {
            int sam = input.OutSamples();
            int lin = input.OutLines();

            double[,] val_in = input.OutVal();
            double[,] val_mask = mask.OutVal();
            double[,] val_out = new double[lin, sam];

            for (int yy = 0; yy < lin; yy++)
            {
                for (int xx = 0; xx < sam; xx++)
                {
                    if (val_mask[yy, xx] != 0)
                    {
                        val_out[yy, xx] = val_in[yy, xx];
                    }
                    else
                    {
                        val_out[yy, xx] = 0;
                    }
                }
            }
            output.InVal(val_out);
        }


    }
}
