﻿using System.Windows.Forms;
using System.IO;

namespace FuzzyCMeans2018
{
    public partial class RSform : Form
    {
        public RSform()
        {
            InitializeComponent();
        }

        /************************************************************************/
        /******************************     宣言     ****************************/
        /************************************************************************/
        #region"宣言"

        ImagePro Function = new ImagePro();

        #endregion

        /************************************************************************/
        /******************************     定数     ****************************/
        /************************************************************************/
        #region"定数"

        const bool ON = true;
        const bool OFF = false;

        #endregion


        /************************************************************************/
        /*************************** グローバル変数群 ***************************/
        /************************************************************************/
        #region"グローバル変数群"

        public string InputFilePath;        //入力データパス
        public string MaskPath;             //マスクパス

        #endregion


        /************************************************************************/
        /************************     フォーム操作関連     **********************/
        /************************************************************************/
        #region"フォーム操作関連"

        //------------------------------------------------------------------------
        // Name             : TextBox_DragEnter
        // Function         : ファイルがドラッグされたときにカーソルを変更
        // Argument			: -
        // Return			: -
        // Update			: 2014/11/04 H.Shirai
        // Comment			: TextBoxにファイルをドラッグ&ドロップ可能にする
        //------------------------------------------------------------------------
        private void TextBox_DragEnter(object sender, DragEventArgs e)
        {
            //ファイルがドラッグされている場合、カーソルを変更する
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }


        //------------------------------------------------------------------------
        // Name             : TextBox_DragDrop
        // Function 		: ドロップされたファイルパスを取得
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/13 K.Matsui
        // Comment			: TextBoxにファイルをドラッグ&ドロップ可能にする
        //------------------------------------------------------------------------
        private void TextBox_DragDrop(object sender, DragEventArgs e)
        {
            //ドロップされたファイルの一覧を取得
            string[] fileName = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            if (fileName.Length <= 0)
            {
                return;
            }

            // ドロップ先がTextBoxであるかチェック
            TextBox txtTarget = sender as TextBox;
            if (txtTarget == null)
            {
                return;
            }

            //TextBoxの内容をファイル名に変更
            if (sender == t_inputdata)
            {
                TextBox_Change(fileName[0], t_inputdata);
                InputFilePath = fileName[0];
            }
            else if (sender == t_maskdata)
            {
                TextBox_Change(fileName[0], t_maskdata);
                MaskPath = fileName[0];
            }
        }


        //------------------------------------------------------------------------
        // Name             : TextBox_Change
        // Function 		: テキストボックスをドロップされたファイルパスに変更
        // Argument			: ファイルパス，対象テキストボックス，対象ディレクトリパスラベル
        // Return			: -
        // Update			: 2018/03/13 K.Matsui
        // Comment			: TextBoxにファイル名を入力する
        //------------------------------------------------------------------------
        private void TextBox_Change(string filename, TextBox t_data)
        {
            string pass = Path.GetFileName(Path.GetDirectoryName(filename));

            t_data.Text = pass + @"\" + Path.GetFileName(filename);
        }

        #endregion


        /************************************************************************/
        /****************************     Main関数     **************************/
        /************************************************************************/
        #region"Main関数"

        //------------------------------------------------------------------------
        // Name             : FCM_Percent
        // Function         : FCM（初期値パーセント入力）を実行
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/23 K.Matsui
        // Comment			: -
        //------------------------------------------------------------------------
        public void FCM_Percent()
        {
            FormEnable(OFF);

            FileData InputData = new FileData();
            FileData MaskData = new FileData();
            FileData OutputData = new FileData();

            InputData.InputData(InputFilePath);
            MaskData.InputData(MaskPath);

            OutputData.CopyInfo(InputData);

            FCM FCM = new FCM(InputData, OutputData, MaskData);
            FCM.LevelSlice(OutputData, MaskData);

            OutputData.WriteBitmap();

            FormEnable(ON);
        }


        //------------------------------------------------------------------------
        // Name             : FCM_Text
        // Function         : FCM（テキストボックス入力）を実行
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/23 K.Matsui
        // Comment			: -
        //------------------------------------------------------------------------
        public void FCM_Text()
        {
            FormEnable(OFF);

            FileData InputData = new FileData();
            FileData MaskData = new FileData();
            FileData OutputData = new FileData();

            InputData.InputData(InputFilePath);
            MaskData.InputData(MaskPath);

            OutputData.CopyInfo(InputData);

            FCM FCM = new FCM(InputData, OutputData, MaskData, t_Cass1.Text, t_Cass2.Text);
            FCM.LevelSlice_Test(OutputData, MaskData);

            OutputData.WriteBitmap();

            FormEnable(ON);
        }


        //------------------------------------------------------------------------
        // Name             : FCM_Quattro
        // Function         : FCM（4クラス）を実行
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/23 K.Matsui
        // Comment			: -
        //------------------------------------------------------------------------
        public void FCM_Quattro()
        {
            FormEnable(OFF);

            FileData InputData = new FileData();
            FileData MaskData = new FileData();
            FileData OutputData = new FileData();

            InputData.InputData(InputFilePath);
            MaskData.InputData(MaskPath);

            OutputData.CopyInfo(InputData);

            FCM FCM = new FCM();
            FCM.FCMQ(InputData, OutputData, MaskData, t_Cass1.Text, t_Cass2.Text);
            FCM.LevelSlice_Test(OutputData, MaskData);

            OutputData.WriteBitmap();
            //OutputData.WriteAscii();



            FormEnable(ON);
        }


        //------------------------------------------------------------------------
        // Name             : FCM_OutputAttHist
        // Function         : FCMの帰属度を出力（Text）
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/23 K.Matsui
        // Comment			: -
        //------------------------------------------------------------------------
        public void FCM_OutputAttHist()
        {
            FormEnable(OFF);

            FileData InputData = new FileData();
            FileData MaskData = new FileData();
            FileData OutputData = new FileData();

            InputData.InputData(InputFilePath);
            MaskData.InputData(MaskPath);

            OutputData.CopyInfo(InputData);

            FCM FCM = new FCM(InputData, OutputData, MaskData, t_Cass1.Text, t_Cass2.Text);

            OutputData.WriteAscii();

            FormEnable(ON);
        }

        public void FormEnable(bool set)
        {
            gb_form.Enabled = set;
        }

        #endregion

        
        /************************************************************************/
        /****************************** イベント群 ******************************/
        /************************************************************************/
        #region"イベント群"

        private void Run_Click(object sender, System.EventArgs e)
        {
            FCM_Quattro();
        }

        private void b_ref_Click(object sender, System.EventArgs e)
        {
            DialogResult dr = fileopen.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                if (sender == b_ref1)
                {
                    TextBox_Change(fileopen.FileName, t_inputdata);
                    InputFilePath = fileopen.FileName;
                }
                else if (sender == b_ref2)
                {
                    TextBox_Change(fileopen.FileName, t_maskdata);
                    MaskPath = fileopen.FileName;
                }

                fileopen.InitialDirectory = Path.GetDirectoryName(fileopen.FileName);
                fileopen.FileName = Path.GetFileName(fileopen.FileName);
            }
        }

        #endregion



    }
}
