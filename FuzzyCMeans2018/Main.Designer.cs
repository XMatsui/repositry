﻿namespace FuzzyCMeans2018
{
    partial class RSform
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.fileopen = new System.Windows.Forms.OpenFileDialog();
            this.t_maskdata = new System.Windows.Forms.TextBox();
            this.t_inputdata = new System.Windows.Forms.TextBox();
            this.b_ref1 = new System.Windows.Forms.Button();
            this.l_maskdata = new System.Windows.Forms.Label();
            this.l_InitialValue = new System.Windows.Forms.Label();
            this.l_inputdata = new System.Windows.Forms.Label();
            this.l_Class1 = new System.Windows.Forms.Label();
            this.l_Class2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.b_ref2 = new System.Windows.Forms.Button();
            this.t_Cass1 = new System.Windows.Forms.TextBox();
            this.t_Cass2 = new System.Windows.Forms.TextBox();
            this.gb_form = new System.Windows.Forms.GroupBox();
            this.gb_form.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileopen
            // 
            this.fileopen.FileName = ".txt";
            this.fileopen.Filter = "*.txt|*.txt|*.*|*.*";
            this.fileopen.InitialDirectory = "C:\\Users\\kmatsui\\Documents\\kmatsui\\";
            this.fileopen.Title = "ファイルを選択してください";
            // 
            // t_maskdata
            // 
            this.t_maskdata.AllowDrop = true;
            this.t_maskdata.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.t_maskdata.Location = new System.Drawing.Point(118, 58);
            this.t_maskdata.Name = "t_maskdata";
            this.t_maskdata.Size = new System.Drawing.Size(326, 20);
            this.t_maskdata.TabIndex = 4;
            this.t_maskdata.DragDrop += new System.Windows.Forms.DragEventHandler(this.TextBox_DragDrop);
            this.t_maskdata.DragEnter += new System.Windows.Forms.DragEventHandler(this.TextBox_DragEnter);
            // 
            // t_inputdata
            // 
            this.t_inputdata.AllowDrop = true;
            this.t_inputdata.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.t_inputdata.Location = new System.Drawing.Point(118, 25);
            this.t_inputdata.Name = "t_inputdata";
            this.t_inputdata.Size = new System.Drawing.Size(326, 20);
            this.t_inputdata.TabIndex = 1;
            this.t_inputdata.DragDrop += new System.Windows.Forms.DragEventHandler(this.TextBox_DragDrop);
            this.t_inputdata.DragEnter += new System.Windows.Forms.DragEventHandler(this.TextBox_DragEnter);
            // 
            // b_ref1
            // 
            this.b_ref1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.b_ref1.Location = new System.Drawing.Point(450, 23);
            this.b_ref1.Name = "b_ref1";
            this.b_ref1.Size = new System.Drawing.Size(75, 23);
            this.b_ref1.TabIndex = 2;
            this.b_ref1.Text = "参照";
            this.b_ref1.UseVisualStyleBackColor = true;
            this.b_ref1.Click += new System.EventHandler(this.b_ref_Click);
            // 
            // l_maskdata
            // 
            this.l_maskdata.AutoSize = true;
            this.l_maskdata.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.l_maskdata.Location = new System.Drawing.Point(4, 59);
            this.l_maskdata.Name = "l_maskdata";
            this.l_maskdata.Size = new System.Drawing.Size(108, 15);
            this.l_maskdata.TabIndex = 3;
            this.l_maskdata.Text = "マスクデータ(txt)：";
            // 
            // l_InitialValue
            // 
            this.l_InitialValue.AutoSize = true;
            this.l_InitialValue.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.l_InitialValue.Location = new System.Drawing.Point(6, 92);
            this.l_InitialValue.Name = "l_InitialValue";
            this.l_InitialValue.Size = new System.Drawing.Size(60, 15);
            this.l_InitialValue.TabIndex = 6;
            this.l_InitialValue.Text = "初期値：";
            // 
            // l_inputdata
            // 
            this.l_inputdata.AutoSize = true;
            this.l_inputdata.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.l_inputdata.Location = new System.Drawing.Point(6, 26);
            this.l_inputdata.Name = "l_inputdata";
            this.l_inputdata.Size = new System.Drawing.Size(106, 15);
            this.l_inputdata.TabIndex = 0;
            this.l_inputdata.Text = "入力データ(txt)：";
            // 
            // l_Class1
            // 
            this.l_Class1.AutoSize = true;
            this.l_Class1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.l_Class1.Location = new System.Drawing.Point(7, 123);
            this.l_Class1.Name = "l_Class1";
            this.l_Class1.Size = new System.Drawing.Size(33, 15);
            this.l_Class1.TabIndex = 7;
            this.l_Class1.Text = "C1：";
            // 
            // l_Class2
            // 
            this.l_Class2.AutoSize = true;
            this.l_Class2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.l_Class2.Location = new System.Drawing.Point(7, 147);
            this.l_Class2.Name = "l_Class2";
            this.l_Class2.Size = new System.Drawing.Size(33, 15);
            this.l_Class2.TabIndex = 9;
            this.l_Class2.Text = "C2：";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.Location = new System.Drawing.Point(418, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 43);
            this.button1.TabIndex = 11;
            this.button1.Text = "実行";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Run_Click);
            // 
            // b_ref2
            // 
            this.b_ref2.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.b_ref2.Location = new System.Drawing.Point(450, 56);
            this.b_ref2.Name = "b_ref2";
            this.b_ref2.Size = new System.Drawing.Size(75, 23);
            this.b_ref2.TabIndex = 5;
            this.b_ref2.Text = "参照";
            this.b_ref2.UseVisualStyleBackColor = true;
            this.b_ref2.Click += new System.EventHandler(this.b_ref_Click);
            // 
            // t_Cass1
            // 
            this.t_Cass1.Location = new System.Drawing.Point(46, 119);
            this.t_Cass1.Name = "t_Cass1";
            this.t_Cass1.Size = new System.Drawing.Size(100, 19);
            this.t_Cass1.TabIndex = 8;
            this.t_Cass1.Text = "16.920";
            this.t_Cass1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // t_Cass2
            // 
            this.t_Cass2.Location = new System.Drawing.Point(46, 143);
            this.t_Cass2.Name = "t_Cass2";
            this.t_Cass2.Size = new System.Drawing.Size(100, 19);
            this.t_Cass2.TabIndex = 10;
            this.t_Cass2.Text = "34.560";
            this.t_Cass2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gb_form
            // 
            this.gb_form.Controls.Add(this.t_Cass2);
            this.gb_form.Controls.Add(this.t_Cass1);
            this.gb_form.Controls.Add(this.b_ref2);
            this.gb_form.Controls.Add(this.button1);
            this.gb_form.Controls.Add(this.l_Class2);
            this.gb_form.Controls.Add(this.l_Class1);
            this.gb_form.Controls.Add(this.l_inputdata);
            this.gb_form.Controls.Add(this.l_InitialValue);
            this.gb_form.Controls.Add(this.l_maskdata);
            this.gb_form.Controls.Add(this.b_ref1);
            this.gb_form.Controls.Add(this.t_inputdata);
            this.gb_form.Controls.Add(this.t_maskdata);
            this.gb_form.Location = new System.Drawing.Point(12, 12);
            this.gb_form.Name = "gb_form";
            this.gb_form.Size = new System.Drawing.Size(531, 177);
            this.gb_form.TabIndex = 0;
            this.gb_form.TabStop = false;
            // 
            // RSform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(555, 203);
            this.Controls.Add(this.gb_form);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "RSform";
            this.RightToLeftLayout = true;
            this.Text = "Remote Sensing ";
            this.gb_form.ResumeLayout(false);
            this.gb_form.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog fileopen;
        private System.Windows.Forms.TextBox t_maskdata;
        private System.Windows.Forms.TextBox t_inputdata;
        private System.Windows.Forms.Button b_ref1;
        private System.Windows.Forms.Label l_maskdata;
        private System.Windows.Forms.Label l_InitialValue;
        private System.Windows.Forms.Label l_inputdata;
        private System.Windows.Forms.Label l_Class1;
        private System.Windows.Forms.Label l_Class2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button b_ref2;
        private System.Windows.Forms.TextBox t_Cass1;
        private System.Windows.Forms.TextBox t_Cass2;
        private System.Windows.Forms.GroupBox gb_form;
    }
}

