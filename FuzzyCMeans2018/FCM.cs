﻿using System;
using System.Linq;
using System.IO;

namespace FuzzyCMeans2018
{
    class FCM
    {
        /************************************************************************/
        /*************************     コンストラクタ     ***********************/
        /************************************************************************/
        #region"コンストラクタ"

        //------------------------------------------------------------------------
        // Name             : FCM
        // Function         : コンストラクタ
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: クラスを生成する際に使用する．
        //------------------------------------------------------------------------
        public FCM()
        {
            CDV = 0.02;
        }

        //------------------------------------------------------------------------
        // Name             : FCM
        // Function         : コンストラクタ
        // Argument			: 入力データ，出力データ，マスクデータ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: CenterValueMeanClass1，2，FuzzyCMeans，LevelSlice
        // Comment			: 平均値により算出された初期値を使用する．
        //------------------------------------------------------------------------
        public FCM(FileData input, FileData output, FileData mask)
        {
            Rep = 100;
            Thr = 0.01;
            Cor = 0;
            Coeff = 5;

            FCM Class1 = new FCM();
            FCM Class2 = new FCM();

            double[] val_in = Function.InArray(input, mask);
            double[] val_tmp = Function.InArray(input, mask);

            val_tmp = Function.ShellSort(val_tmp);

            CenterValueMeanClass1(Class1, val_tmp);
            CenterValueMeanClass2(Class2, val_tmp);

            FuzzyCMeans(Class1, Class2, val_in);

            Function.OutArray(output, mask, Class2.DefVal);
        }

        //------------------------------------------------------------------------
        // Name             : FCM
        // Function         : コンストラクタ
        // Argument			: 入力データ，出力データ，マスクデータ，C1初期値，C2初期値
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: CenterValueMeanClass1，2，FuzzyCMeans，LevelSlice
        // Comment			: 入力された初期値を使用する．
        //------------------------------------------------------------------------
        public FCM(FileData input, FileData output, FileData mask, string ccv1, string ccv2)
        {
            Rep = 100;
            Thr = 0.01;
            Cor = 0;
            Coeff = 5;

            FCM Class1 = new FCM();
            FCM Class2 = new FCM();

            double[] val_in = Function.InArray(input, mask);
            double[] val_tmp = Function.InArray(input, mask);

            val_tmp = Function.ShellSort(val_tmp);

            Class1.CCV = double.Parse(ccv1);
            Class2.CCV = double.Parse(ccv2);

            FuzzyCMeans(Class1, Class2, val_in);

            Function.OutArray(output, mask, Class2.AttVal);
        }

        #endregion


        /************************************************************************/
        /******************************     宣言     ****************************/
        /************************************************************************/
        #region"宣言"

        ImagePro Function = new ImagePro();

        #endregion


        /************************************************************************/
        /******************************     定数     ****************************/
        /************************************************************************/
        #region"定数"

        const int RED = 0;
        const int GREEN = 1;
        const int BLUE = 2;
        const int RGB = 3;

        const int LEVEL = 6;
        
        //レベル別出力画像色
        //紫
        const int LEVEL_1_RED = 125;
        const int LEVEL_1_GREEN = 0;
        const int LEVEL_1_BLUE = 255;
        //青
        const int LEVEL_2_RED = 0;
        const int LEVEL_2_GREEN = 0;
        const int LEVEL_2_BLUE = 255;
        //水色
        const int LEVEL_3_RED = 0;
        const int LEVEL_3_GREEN = 255;
        const int LEVEL_3_BLUE = 255;
        //黄緑
        const int LEVEL_4_RED = 0;
        const int LEVEL_4_GREEN = 255;
        const int LEVEL_4_BLUE = 0;
        //黄色
        const int LEVEL_5_RED = 255;
        const int LEVEL_5_GREEN = 255;
        const int LEVEL_5_BLUE = 0;
        //オレンジ
        const int LEVEL_6_RED = 255;
        const int LEVEL_6_GREEN = 125;
        const int LEVEL_6_BLUE = 0;
        //赤
        const int LEVEL_7_RED = 255;
        const int LEVEL_7_GREEN = 0;
        const int LEVEL_7_BLUE = 0;
        //白
        const int LEVEL_8_RED = 255;
        const int LEVEL_8_GREEN = 255;
        const int LEVEL_8_BLUE = 255;
        
        //マスク
        const int MASK_RED = 0;
        const int MASK_GREEN = 0;
        const int MASK_BLUE = 0;

        #endregion


        /************************************************************************/
        /******************************     変数     ****************************/
        /************************************************************************/
        #region"変数"
        
        //条件
        private int Rep;                    //反復数
        private double Thr;                 //終了条件
        private int Cor;                    //補正値
        private double Coeff;               //重み係数        

        private double CDV;                 //クラスの初期値(ClassDefaultValue)
        private double CCV;                 //クラスの中心値(ClassCenterValue)
        private double[] AttVal;            //クラスの帰属値
        private double[] DefVal;            //クラスの確定値

        private double[] AttClass;          //所属クラス

        #endregion


        /************************************************************************/
        /***************************     初期値決定法     ***********************/
        /************************************************************************/
        #region"初期値決定法"

        //------------------------------------------------------------------------
        // Name             : FCM_CenterValueMeanClass1
        // Function         : Fuzzy c-means法における初期値を算出
        // Argument			: 対象クラス，数値データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CenterValueMeanClass1(FCM tagc, double[] val_in)
        {
            int len = val_in.Length;
            double sum = 0;
            double width = len * tagc.CDV;

            for (int ii = 0; ii < width; ii++)
            {
                sum += val_in[ii];
            }

            tagc.CCV = sum / width;
        }

        //------------------------------------------------------------------------
        // Name             : FCM_CenterValueMeanClass2
        // Function         : Fuzzy c-means法における初期値を算出
        // Argument			: 対象クラス，数値データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CenterValueMeanClass2(FCM tagc, double[] val_in)
        {
            int len = val_in.Length;
            double sum = 0;
            double width = len * tagc.CDV;

            for (int ii = len - 1; len - width < ii; ii--)
            {
                sum += val_in[ii];
            }

            tagc.CCV = sum / width;
        }

        #endregion


        /************************************************************************/
        /************************     Fuzzy c-means処理     *********************/
        /************************************************************************/
        #region"Fuzzy c-means処理"

        //------------------------------------------------------------------------
        // Name             : CulDefineValue_B
        // Function         : 確定値の算出
        // Argument			: クラス1データ，クラス2データ，入力データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: C1以下のDN値は帰属度を"0"に，C2以上のDN値は確定値度を"255"に
        //------------------------------------------------------------------------
        public void CulDefineValue_B(FCM c1, FCM c2, double[] val_in)
        {
            int len = val_in.Length;

            for (int ii = 0; ii <len; ii++)
            {
                if (val_in[ii] < c1.CCV)
                {
                    c2.DefVal[ii] = 0;
                }
                else if (c2.CCV < val_in[ii])
                {
                    c2.DefVal[ii] = 255;
                }
                else
                {
                    c2.DefVal[ii] = c2.AttVal[ii] * 255;
                }
            }
        }


        //------------------------------------------------------------------------
        // Name             : CulDefineValue_A
        // Function         : 確定値の算出
        // Argument			: クラス1データ，クラス2データ，入力データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CulDefineValue_A(FCM c1, FCM c2, double[] val_in)
        {
            int len = val_in.Length;

            for (int ii = 0; ii < len; ii++)
            {
                    c2.DefVal[ii] = c2.AttVal[ii] * 255;
            }
        }


        //------------------------------------------------------------------------
        // Name             : UpdateCenter
        // Function         : クラス中央値の更新
        // Argument			: クラス1データ，クラス2データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void UpdateCenter(FCM c1, FCM c2, double[] val_in)
        {
            int len = val_in.Length;

            double tmp;                             //クラス中央値の途中計算
            double NMR1 = 0, NMR2 = 0;              //分子
            double DNM1 = 0, DNM2 = 0;              //分母

            //クラスの中心を更新
            for (int ii = 0; ii < len; ii++)
            {
                if (AttClass[ii] == 1)                      //クラス1に所属
                {
                    tmp = Math.Round((Math.Pow(c1.AttVal[ii], Coeff)), 13);
                    DNM1 = DNM1 + tmp;
                    NMR1 = NMR1 + Math.Round(tmp * val_in[ii], 13);
                }
                else if (AttClass[ii] == 2)                 //クラス2に所属
                {
                    tmp = Math.Round((Math.Pow(c2.AttVal[ii], Coeff)), 13);
                    DNM2 = DNM2 + tmp;
                    NMR2 = NMR2 + Math.Round(tmp * val_in[ii], 13);
                }
            }

            //中心値を更新
            c1.CCV = Math.Round(NMR1 / DNM1, 13);
            c2.CCV = Math.Round(NMR2 / DNM2, 13);
        }


        //------------------------------------------------------------------------
        // Name             : CalAttributeValue
        // Function         : 帰属度の算出
        // Argument			: クラス1データ，クラス2データ，数値データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CulAttributeValue(FCM c1, FCM c2, double[] val_in)
        {
            int len = val_in.Length;

            double Dst1, Dst2;                      //非類似度
            double tmp1, tmp2;                      //帰属度算出途中計算

            for (int ii = 0; ii < len; ii++)
            {
                if (c1.CCV == val_in[ii])
                {
                    c1.AttVal[ii] = 1;
                    c2.AttVal[ii] = 0;
                    AttClass[ii] = 1;
                }
                else if (c2.CCV == val_in[ii])
                {
                    c1.AttVal[ii] = 0;
                    c2.AttVal[ii] = 1;
                    AttClass[ii] = 2;
                }
                else
                {
                    Dst1 = Math.Round(Math.Abs(val_in[ii] - c1.CCV), 13);
                    Dst2 = Math.Round(Math.Abs(val_in[ii] - c2.CCV), 13);


                    //帰属度の途中計算
                    tmp1 = Math.Round((1 / (Math.Pow((Dst1 / Dst2), 1.000000 / (Coeff - 1)))), 13);
                    tmp2 = Math.Round((1 / (Math.Pow((Dst2 / Dst1), 1.000000 / (Coeff - 1)))), 13);

                    //帰属度算出
                    c1.AttVal[ii] = Math.Round(tmp1 / (tmp1 + tmp2), 3);
                    c2.AttVal[ii] = Math.Round(tmp2 / (tmp1 + tmp2), 3);

                    if (c2.AttVal[ii] < c1.AttVal[ii])      //帰属度の高いほうに所属
                    {
                        AttClass[ii] = 1;
                    }
                    else
                    {
                        AttClass[ii] = 2;
                    }
                }
            }
        }


        //------------------------------------------------------------------------
        // Name             : FuzzyCMeans
        // Function         : Main関数
        // Argument			: 入力データ，出力データ，マスクデータ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: CulAttributeValue，UpdataCenter，CulDefineValue
        // Comment			: -
        //------------------------------------------------------------------------
        public void FuzzyCMeans(FCM c1, FCM c2,double[] val_in)
        {
            int len = val_in.Length;

            c1.AttVal = new double[len];
            c2.AttVal = new double[len];
            c1.DefVal = new double[len];
            c2.DefVal = new double[len];

            AttClass = new double[len];
            double[] PreAttClass = new double[len];

            int classcnt = 0;                       //クラス中央値の変化数
            double changecnt = 0;                   //クラス変化率
            double end = len * Thr / 100;           //終了条件


            for (int ii = 0; ii < Rep; ii++)
            {
                CulAttributeValue(c1, c2, val_in);
                UpdateCenter(c1, c2, val_in);

                if (0 < ii)
                {
                    for (int jj = 0; jj < len; jj++)
                    {
                        if (PreAttClass[jj] != AttClass[jj])
                        {
                            classcnt++;
                        }
                    }

                    changecnt = classcnt / len * 100;

                    if (changecnt < end)
                    {
                        break;
                    }
                }
                else { }

                PreAttClass = AttClass;
                classcnt = 0;
            }
            CulDefineValue_B(c1, c2, val_in);
        }

        #endregion


        /************************************************************************/
        /************************     Fuzzy c-meansQ処理     *********************/
        /************************************************************************/
        #region"Fuzzy c-means処理"

        //------------------------------------------------------------------------
        // Name             : CulDefineValueQ
        // Function         : 確定値の算出
        // Argument			: クラス1データ，クラス2データ，入力データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: C1以下のDN値は帰属度を"0"に，C2以上のDN値は確定値度を"255"に
        //------------------------------------------------------------------------
        public void CulDefineValueQ(FCM c1, FCM c2, FCM c3, FCM c4, double[] val_in)
        {
            int len = val_in.Length;

            for (int ii = 0; ii < len; ii++)
            {
                if (val_in[ii] < c1.CCV)
                {
                    c2.AttVal[ii] = 0;
                }
                else if (c2.CCV < val_in[ii])
                {
                    c2.AttVal[ii] = 1;
                }
                else
                {
                }
            }
        }


        //------------------------------------------------------------------------
        // Name             : UpdateCenterQ
        // Function         : クラス中央値の更新
        // Argument			: クラス1データ，クラス2データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void UpdateCenterQ(FCM c1, FCM c2, FCM c3, FCM c4, double[] val_in)
        {
            int len = val_in.Length;

            double tmp;                             //クラス中央値の途中計算
            double NMR1 = 0, NMR2 = 0, NMR3 = 0, NMR4 = 0;              //分子
            double DNM1 = 0, DNM2 = 0, DNM3 = 0, DNM4 = 0;              //分母

            //クラスの中心を更新
            for (int ii = 0; ii < len; ii++)
            {
                if (AttClass[ii] == 1)                      //クラス1に所属
                {
                    tmp = Math.Round((Math.Pow(c1.AttVal[ii], Coeff)), 13);
                    DNM1 = DNM1 + tmp;
                    NMR1 = NMR1 + Math.Round(tmp * val_in[ii], 13);
                }
                else if (AttClass[ii] == 2)                 //クラス2に所属
                {
                    tmp = Math.Round((Math.Pow(c2.AttVal[ii], Coeff)), 13);
                    DNM2 = DNM2 + tmp;
                    NMR2 = NMR2 + Math.Round(tmp * val_in[ii], 13);
                }
                else if (AttClass[ii] == 3)                 //クラス2に所属
                {
                    tmp = Math.Round((Math.Pow(c3.AttVal[ii], Coeff)), 13);
                    DNM3 = DNM3 + tmp;
                    NMR3 = NMR3 + Math.Round(tmp * val_in[ii], 13);
                }
                else if (AttClass[ii] == 4)                 //クラス2に所属
                {
                    tmp = Math.Round((Math.Pow(c4.AttVal[ii], Coeff)), 13);
                    DNM4 = DNM4 + tmp;
                    NMR4 = NMR4 + Math.Round(tmp * val_in[ii], 13);
                }
            }

            //中心値を更新
            c1.CCV = Math.Round(NMR1 / DNM1, 13);
            c2.CCV = Math.Round(NMR2 / DNM2, 13);
            c3.CCV = Math.Round(NMR3 / DNM3, 13);
            c4.CCV = Math.Round(NMR4 / DNM4, 13);
        }


        //------------------------------------------------------------------------
        // Name             : CalAttributeValueQ
        // Function         : 帰属度の算出
        // Argument			: クラス1データ，クラス2データ，数値データ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CulAttributeValueQ(FCM c1, FCM c2, FCM c3, FCM c4, double[] val_in)
        {
            int len = val_in.Length;

            double Dst1, Dst2, Dst3, Dst4;                      //非類似度
            double tmp1, tmp2, tmp3, tmp4;                      //帰属度算出途中計算

            for (int ii = 0; ii < len; ii++)
            {
                if (c1.CCV == val_in[ii])
                {
                    c1.AttVal[ii] = 1;
                    c2.AttVal[ii] = 0;
                    c3.AttVal[ii] = 0;
                    c4.AttVal[ii] = 0;
                    AttClass[ii] = 1;
                }
                else if (c2.CCV == val_in[ii])
                {
                    c1.AttVal[ii] = 0;
                    c2.AttVal[ii] = 1;
                    c3.AttVal[ii] = 0;
                    c4.AttVal[ii] = 0;
                    AttClass[ii] = 2;
                }else if (c3.CCV == val_in[ii])
                {
                    c1.AttVal[ii] = 0;
                    c2.AttVal[ii] = 0;
                    c3.AttVal[ii] = 1;
                    c4.AttVal[ii] = 0;
                    AttClass[ii] = 3;
                }
                else if (c4.CCV == val_in[ii])
                {
                    c1.AttVal[ii] = 0;
                    c2.AttVal[ii] = 0;
                    c3.AttVal[ii] = 0;
                    c4.AttVal[ii] = 1;
                    AttClass[ii] = 4;
                }
                else
                {
                    Dst1 = Math.Round(Math.Abs(val_in[ii] - c1.CCV), 13);
                    Dst2 = Math.Round(Math.Abs(val_in[ii] - c2.CCV), 13);
                    Dst3 = Math.Round(Math.Abs(val_in[ii] - c3.CCV), 13);
                    Dst4 = Math.Round(Math.Abs(val_in[ii] - c4.CCV), 13);

                    double ttt1 = Math.Pow((Dst1 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst1 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst1 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst1 / Dst4), 1.000000 / (Coeff - 1));
                    double ttt2 = Math.Pow((Dst2 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst2 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst2 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst2 / Dst4), 1.000000 / (Coeff - 1));
                    double ttt3 = Math.Pow((Dst3 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst3 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst3 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst3 / Dst4), 1.000000 / (Coeff - 1));
                    double ttt4 = Math.Pow((Dst4 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst4 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst4 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst4 / Dst4), 1.000000 / (Coeff - 1));

                    //帰属度の途中計算
                    tmp1 = Math.Round((1 / ttt1), 13);
                    tmp2 = Math.Round((1 / ttt2), 13);
                    tmp3 = Math.Round((1 / ttt3), 13);
                    tmp4 = Math.Round((1 / ttt4), 13);

                    //帰属度算出
                    c1.AttVal[ii] = Math.Round(tmp1, 10);
                    c2.AttVal[ii] = Math.Round(tmp2, 10);
                    c3.AttVal[ii] = Math.Round(tmp3, 10);
                    c4.AttVal[ii] = Math.Round(tmp4, 10);

                    if (c2.AttVal[ii] < c1.AttVal[ii] && c3.AttVal[ii] < c1.AttVal[ii] && c4.AttVal[ii] < c1.AttVal[ii])      //帰属度の高いほうに所属
                    {
                        AttClass[ii] = 1;
                    }
                    else if (c1.AttVal[ii] < c2.AttVal[ii] && c3.AttVal[ii] < c2.AttVal[ii] && c4.AttVal[ii] < c2.AttVal[ii])
                    {
                        AttClass[ii] = 2;
                    }
                    else if (c1.AttVal[ii] < c3.AttVal[ii] && c2.AttVal[ii] < c3.AttVal[ii] && c4.AttVal[ii] < c3.AttVal[ii])
                    {
                        AttClass[ii] = 3;
                    }
                    else if (c1.AttVal[ii] < c4.AttVal[ii] && c3.AttVal[ii] < c4.AttVal[ii] && c3.AttVal[ii] < c4.AttVal[ii])
                    {
                        AttClass[ii] = 4;
                    }
                }
            }
        }


        //------------------------------------------------------------------------
        // Name             : FuzzyCMeansQ
        // Function         : Main関数
        // Argument			: 入力データ，出力データ，マスクデータ
        // Return			: -
        // Update			: 2018/03/18 K.Matsui
        // Refarences		: CulAttributeValue，UpdataCenter，CulDefineValue
        // Comment			: -
        //------------------------------------------------------------------------
        public void FuzzyCMeansQ(FCM c1, FCM c2, FCM c3, FCM c4, double[] val_in)
        {
            int len = val_in.Length;

            c1.AttVal = new double[len];
            c2.AttVal = new double[len];
            c3.AttVal = new double[len];
            c4.AttVal = new double[len];
            c1.DefVal = new double[len];
            c2.DefVal = new double[len];
            c3.DefVal = new double[len];
            c4.DefVal = new double[len];

            AttClass = new double[len];
            double[] PreAttClass = new double[len];

            int classcnt = 0;                       //クラス中央値の変化数
            double changecnt = 0;                   //クラス変化率
            double end = len * Thr / 100;           //終了条件


            for (int ii = 0; ii < Rep; ii++)
            {
                CulAttributeValueQ(c1, c2, c3, c4, val_in);
                UpdateCenterQ(c1, c2, c3, c4, val_in);

                if (0 < ii)
                {
                    for (int jj = 0; jj < len; jj++)
                    {
                        if (PreAttClass[jj] != AttClass[jj])
                        {
                            classcnt++;
                        }
                    }

                    changecnt = classcnt / len * 100;

                    if (changecnt < end)
                    {
                        break;
                    }
                }
                else { }

                PreAttClass = AttClass;
                classcnt = 0;
                changecnt = 0;
            }
            CulDefineValueQ(c1, c2, c3, c4, val_in);
        }

        #endregion


        /************************************************************************/
        /***********************     レベルスライス処理     *********************/
        /************************************************************************/
        #region"レベルスライス処理"

        //------------------------------------------------------------------------
        // Name             : LevelSlice_Test
        // Function         : レベルスライス処理
        // Argument			: 確定値(クラス2)，出力画像データ
        // Return			: -
        // Update			: 2016/11/16 K.Matsui
        // Comment			: 試験的なメソッド
        //------------------------------------------------------------------------
        public void LevelSlice_Test(FileData output, FileData mask)
        {
            int sam = output.OutSamples();
            int lin = output.OutLines();

            double[,] val_mask = mask.OutVal();
            double[,] val = output.OutVal();
            int[, ,] val_image = new int[RGB, lin, sam];

            double[] th = new double[LEVEL];
            th[0] = 0;
            th[1] = 0.005745;
            th[2] = 0.096564;
            th[3] = 0.154017;
            th[4] = 0.211469;
            th[5] = 0.268921;

            for (int ii = 0; ii < lin; ii++)
            {
                for (int jj = 0; jj < sam; jj++)
                {
                    if (val_mask[ii, jj] == 0)
                    {
                        //MASK
                        val_image[RED, ii, jj] = MASK_RED;
                        val_image[GREEN, ii, jj] = MASK_GREEN;
                        val_image[BLUE, ii, jj] = MASK_BLUE;
                    }
                    else if (val[ii, jj] <= th[1])
                    {
                        //LEVEL_2
                        val_image[RED, ii, jj] = LEVEL_2_RED;
                        val_image[GREEN, ii, jj] = LEVEL_2_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_2_BLUE;
                    }
                    else if (th[1] < val[ii, jj] && val[ii, jj] <= th[2])
                    {
                        //LEVEL_4
                        val_image[RED, ii, jj] = LEVEL_4_RED;
                        val_image[GREEN, ii, jj] = LEVEL_4_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_4_BLUE;
                    }
                    else if (th[2] < val[ii, jj] && val[ii, jj] <= th[3])
                    {
                        //LEVEL_5
                        val_image[RED, ii, jj] = LEVEL_5_RED;
                        val_image[GREEN, ii, jj] = LEVEL_5_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_5_BLUE;
                    }
                    else if (th[3] < val[ii, jj] && val[ii, jj] <= th[4])
                    {
                        //LEVEL_6
                        val_image[RED, ii, jj] = LEVEL_6_RED;
                        val_image[GREEN, ii, jj] = LEVEL_6_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_6_BLUE;
                    }
                    else if (th[4] < val[ii, jj] && val[ii, jj] <= th[5])
                    {
                        //LEVEL_7
                        val_image[RED, ii, jj] = LEVEL_7_RED;
                        val_image[GREEN, ii, jj] = LEVEL_7_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_7_BLUE;
                    }
                    else if (th[5] < val[ii, jj])
                    {
                        //LEVEL_8
                        val_image[RED, ii, jj] = LEVEL_8_RED;
                        val_image[GREEN, ii, jj] = LEVEL_8_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_8_BLUE;
                    }
                    else
                    {
                        //Error
                        val_image[RED, ii, jj] = MASK_RED;
                        val_image[GREEN, ii, jj] = MASK_GREEN;
                        val_image[BLUE, ii, jj] = MASK_BLUE;
                    }
                }
            }
            output.InBitmap(val_image);
        }

        
        //------------------------------------------------------------------------
        // Name             : LevelSlice
        // Function         : レベルスライス処理
        // Argument			: 確定値(クラス2)，出力画像データ
        // Return			: -
        // Update			: 2016/11/16 K.Matsui
        // Comment			: -
        //------------------------------------------------------------------------
        public void LevelSlice(FileData output, FileData mask)
        {
            int sam = output.OutSamples();
            int lin = output.OutLines();

            double[,] val_mask = mask.OutVal();
            double[,] val = output.OutVal();
            int[, ,] val_image = new int[RGB, lin, sam];
            
            for (int ii = 0; ii < lin; ii++)
            {
                for (int jj = 0; jj < sam; jj++)
                {
                    if (val_mask[ii, jj] == 0)
                    {
                        //MASK
                        val_image[RED, ii, jj] = MASK_RED;
                        val_image[GREEN, ii, jj] = MASK_GREEN;
                        val_image[BLUE, ii, jj] = MASK_BLUE;
                    }
                    else if (0 <= val[ii, jj] && val[ii, jj] <= 43)
                    {
                        //LEVEL_2
                        val_image[RED, ii, jj] = LEVEL_2_RED;
                        val_image[GREEN, ii, jj] = LEVEL_2_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_2_BLUE;
                    }
                    else if (43 < val[ii, jj] && val[ii, jj] <= 86)
                    {
                        //LEVEL_4
                        val_image[RED, ii, jj] = LEVEL_4_RED;
                        val_image[GREEN, ii, jj] = LEVEL_4_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_4_BLUE;
                    }
                    else if (86 < val[ii, jj] && val[ii, jj] <= 129)
                    {
                        //LEVEL_5
                        val_image[RED, ii, jj] = LEVEL_5_RED;
                        val_image[GREEN, ii, jj] = LEVEL_5_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_5_BLUE;
                    }
                    else if (129 < val[ii, jj] && val[ii, jj] <= 172)
                    {
                        //LEVEL_6
                        val_image[RED, ii, jj] = LEVEL_6_RED;
                        val_image[GREEN, ii, jj] = LEVEL_6_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_6_BLUE;
                    }
                    else if (172 < val[ii, jj] && val[ii, jj] <= 215)
                    {
                        //LEVEL_7
                        val_image[RED, ii, jj] = LEVEL_7_RED;
                        val_image[GREEN, ii, jj] = LEVEL_7_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_7_BLUE;
                    }
                    else if (215 < val[ii, jj])
                    {
                        //LEVEL_8
                        val_image[RED, ii, jj] = LEVEL_8_RED;
                        val_image[GREEN, ii, jj] = LEVEL_8_GREEN;
                        val_image[BLUE, ii, jj] = LEVEL_8_BLUE;
                    }
                    else
                    {
                        //Error
                        val_image[RED, ii, jj] = MASK_RED;
                        val_image[GREEN, ii, jj] = MASK_GREEN;
                        val_image[BLUE, ii, jj] = MASK_BLUE;
                    }
                }
            }
            output.InBitmap(val_image);
        }

        #endregion


        /************************************************************************/
        /****************************     変数操作     **************************/
        /************************************************************************/
        #region"変数操作"
        #endregion



        public void FCMQ(FileData input, FileData output, FileData mask, string ccv1, string ccv2)
        {
            Rep = 100;
            Thr = 0.01;
            Cor = 0;
            Coeff = 3;

            FCM Class1 = new FCM();
            FCM Class2 = new FCM();
            FCM Class3 = new FCM();
            FCM Class4 = new FCM();

            double[] val_in = Function.InArray(input, mask);
            double[] val_tmp = Function.InArray(input, mask);

            val_tmp = Function.ShellSort(val_tmp);
                        
            CenterValueMeanClass1(Class1, val_tmp);
            CenterValueMeanClass2(Class2, val_tmp);
            Class3.CCV = double.Parse(ccv1);
            Class4.CCV = double.Parse(ccv2);

            FuzzyCMeansQ(Class1, Class2, Class3, Class4, val_in);

            Function.OutArray(output, mask, Class2.AttVal);
            input.WriteAttHist(mask, Class4.AttVal);
            //Message(Class1, Class2, Class3, Class4, ccv1, ccv2);
        }

        public void Message(FCM c1, FCM c2, FCM c3, FCM c4, string ccv1, string ccv2)
        {
            double[] num = new double[2];
            string[] attval = new string[2];
            double Dst1, Dst2, Dst3, Dst4;                      //非類似度
            double tmp1, tmp2, tmp3, tmp4;                      //帰属度算出途中計算

            for (int ii = 0; ii < 2; ii++)
            {

                if (ii == 0) { num[ii] = Math.Round(Double.Parse(ccv1), 13); }
                else { num[ii] = Math.Round(Double.Parse(ccv2), 13); }

                Dst1 = Math.Round(Math.Abs(num[ii] - c1.CCV), 13);
                Dst2 = Math.Round(Math.Abs(num[ii] - c2.CCV), 13);
                Dst3 = Math.Round(Math.Abs(num[ii] - c3.CCV), 13);
                Dst4 = Math.Round(Math.Abs(num[ii] - c4.CCV), 13);

                double ttt1 = Math.Pow((Dst1 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst1 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst1 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst1 / Dst4), 1.000000 / (Coeff - 1));
                double ttt2 = Math.Pow((Dst2 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst2 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst2 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst2 / Dst4), 1.000000 / (Coeff - 1));
                double ttt3 = Math.Pow((Dst3 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst3 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst3 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst3 / Dst4), 1.000000 / (Coeff - 1));
                double ttt4 = Math.Pow((Dst4 / Dst1), 1.000000 / (Coeff - 1)) + Math.Pow((Dst4 / Dst2), 1.000000 / (Coeff - 1)) + Math.Pow((Dst4 / Dst3), 1.000000 / (Coeff - 1)) + Math.Pow((Dst4 / Dst4), 1.000000 / (Coeff - 1));

                //帰属度の途中計算
                tmp1 = Math.Round((1 / ttt1), 10);
                tmp2 = Math.Round((1 / ttt2), 10);
                tmp3 = Math.Round((1 / ttt3), 10);
                tmp4 = Math.Round((1 / ttt4), 10);

                attval[ii] = tmp4.ToString();
            }

            double numnum = 0;

        }
    }
}