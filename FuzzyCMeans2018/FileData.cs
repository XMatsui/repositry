﻿using System;
using System.IO;
using System.Text;
using System.Drawing;

namespace FuzzyCMeans2018
{
    class FileData
    {
        /************************************************************************/
        /******************************     定数     ****************************/
        /************************************************************************/
        #region"定数"

        const int RED = 0;
        const int GREEN = 1;
        const int BLUE = 2;
        const int RGB = 3;

        #endregion


        /************************************************************************/
        /******************************     変数     ****************************/
        /************************************************************************/
        #region"変数"

        private int samples;        //画像サイズ(X軸)
        private int lines;          //画像サイズ(Y軸)
        private int size;           //総画素数

        private bool head;          //ヘッダ有無
        private int header_num;     //ヘッダ数
        private string[] header;    //ヘッダデータ

        private string filepass;    //ファイルパス
        private string dirpass;     //ディレクトリパス
        private string filename;    //ファイル名

        private double[,] val;      //数値データ
        private int[, ,] bitmap;    //画素値データ

        #endregion


        /************************************************************************/
        /****************************     入力関連     **************************/
        /************************************************************************/
        #region"入力関連"

        //------------------------------------------------------------------------
        // Name             : InputData
        // Function         : 入力クラスのメイン
        // Argument			: ファイルパス
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: InputValue
        // Comment			: InputValueとHeaderCheckのメソッドを分離させるべき
        //------------------------------------------------------------------------
        public void InputData(string pass)
        {
            filepass = pass;
            dirpass = Path.GetDirectoryName(pass);
            filename = Path.GetFileNameWithoutExtension(pass);

            InputValue();
        }


        //------------------------------------------------------------------------
        // Name             : Inputvalue
        // Function         : 入力データ読み取り
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: HeaderCheck
        // Comment			: ヘッダーチェックを独立にしたい．
        //------------------------------------------------------------------------
        public void InputValue()
        {
            string rawdata;
            StreamReader sr = new StreamReader(filepass, Encoding.GetEncoding("ASCII"));

            rawdata = HeaderCheck(sr);
            val = new double[lines, samples];

            for (int yy = 0; yy < lines; yy++)
            {
                int xx = 0;
                rawdata = rawdata.Trim();
                string[] tmpdata = rawdata.Split();

                for (int ii = 0; ii < tmpdata.Length; ii++)
                {
                    if (tmpdata[ii] != "")
                    {
                        val[yy, xx] = double.Parse(tmpdata[ii]);
                        xx++;
                    }
                    else { }
                }
                rawdata = sr.ReadLine();
            }

            sr.Close();
        }


        //------------------------------------------------------------------------
        // Name             : HeaderCheck
        // Function         : ヘッダの有無を調べる
        // Argument			: 読み取りファイル
        // Return			: 数値データの1行目
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: SetPropaty
        // Comment			: Enviファイルを対象として作成してます．
        //------------------------------------------------------------------------
        public string HeaderCheck(StreamReader tar)
        {
            int cnt = 0;
            string[] head_tmp = new string[32];

            //
            head_tmp[0] = tar.ReadLine();

            if (head_tmp[cnt].Substring(0, 1) == ";")
                head = true;
            else
                head = false;
            //
            //
            while (head)
            {
                if (head_tmp[cnt].Substring(0, 1) == ";")
                {
                    cnt++;
                    head_tmp[cnt] = tar.ReadLine();
                }
                else { break; }
            }
            //

            SetPropaty(head_tmp, cnt);

            return head_tmp[cnt];
        }


        //------------------------------------------------------------------------
        // Name             : SetPropaty
        // Function         : データ情報を格納
        // Argument			: ヘッダデータ，ヘッダ数
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: Enviファイルを対象として作成してます．
        //------------------------------------------------------------------------
        public void SetPropaty(string[] tar, int num)
        {
            string[] str_tmp;

            header_num = num;
            header = new string[num];
            for (int ii = 0; ii < num; ii++)
            {
                header[ii] = tar[ii];
            }

            str_tmp = header[2].Split();

            samples = Int32.Parse(str_tmp[3]);
            lines = Int32.Parse(str_tmp[6]);
            size = samples * lines;
        }

        #endregion


        /************************************************************************/
        /****************************     出力関連     **************************/
        /************************************************************************/
        #region"出力関連"

        //------------------------------------------------------------------------
        // Name             : ToBitmap
        // Function         : 数値データをRGBに入力
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void ToBitmap()
        {
            bitmap = new Int32[RGB, lines, samples];

            for (int yy = 0; yy < lines; yy++)
            {
                for (int xx = 0; xx < samples; xx++)
                {
                    bitmap[RED, yy, xx] = (int)val[yy, xx];
                    bitmap[GREEN, yy, xx] = (int)val[yy, xx];
                    bitmap[BLUE, yy, xx] = (int)val[yy, xx];
                }
            }
        }


        //------------------------------------------------------------------------
        // Name             : WriteBitmap
        // Function         : ビットマップデータを書き出す
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void WriteBitmap()
        {
            Bitmap OutputBitmap = new Bitmap(samples, lines);

            for (int yy = 0; yy < lines; yy++)
            {
                for (int xx = 0; xx < samples; xx++)
                {
                    Color BmpColor = Color.FromArgb(bitmap[RED, yy, xx], bitmap[GREEN, yy, xx], bitmap[BLUE, yy, xx]);
                    OutputBitmap.SetPixel(xx, yy, BmpColor);
                }
            }
            
            OutputBitmap.Save(dirpass + "\\" + filename + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

            OutputBitmap.Dispose();
        }


        //------------------------------------------------------------------------
        // Name             : WriteAscii
        // Function         : ASCIIデータを書き出す
        // Argument			: -
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void WriteAscii()
        {
            StreamWriter sw = new StreamWriter(dirpass+ "\\" + filename + "_ascii.txt", false, Encoding.GetEncoding("ASCII"));
            
            if (head)
            {
                for (int ii = 0; ii < header_num; ii++)
                {
                    sw.WriteLine(header[ii]);
                }
            }
            else { }

            for (int yy = 0; yy < lines; yy++)
            {
                for (int xx = 0; xx < samples; xx++)
                {
                    sw.Write("\t" + Convert.ToString(val[yy, xx]));
                }
                sw.Write("\n");
            }

            sw.Close();
        }

        public void WriteAttHist(FileData mask, double[] att)
        {
            double[] OutAtt = new double[256];
            int cnt =0;

            StreamWriter sw = new StreamWriter(dirpass + "\\" + filename + "_Atthist.txt", false, Encoding.GetEncoding("ASCII"));

            for (int ii = 0; ii < lines; ii++)
            {
                for (int jj = 0; jj < samples; jj++)
                {
                    if (mask.val[ii,jj] != 0)
                    {
                        int num = (int)val[ii, jj];

                        OutAtt[num] = att[cnt];
                        cnt++;
                    }
                }
            }


            //結果の書き込み
            for (int ii = 0; ii < 256; ii++)
            {
                sw.Write(Convert.ToString(OutAtt[ii]) + "\n");
            }


            sw.Close();
        }

        #endregion


        /************************************************************************/
        /****************************     変数操作     **************************/
        /************************************************************************/
        #region"変数操作"

        //------------------------------------------------------------------------
        // Name             : AddText
        // Function         : ファイル名に文字を追加
        // Argument			: 追加する文字
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void AddText(string text)
        {
            filename += text;
        }


        //------------------------------------------------------------------------
        // Name             : InBitmap
        // Function         : 数値データを入力
        // Argument			: 入力する数値データ
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void InBitmap(int[,,] val_in)
        {
            bitmap = val_in;
        }


        //------------------------------------------------------------------------
        // Name             : CopyInfo
        // Function         : FileData情報をコピー
        // Argument			: コピー先のFileData
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CopyInfo(FileData tar)
        {
            samples = tar.samples;
            lines = tar.lines;
            size = tar.size;

            head = tar.head;

            filepass = tar.filepass;
            dirpass = tar.dirpass;
            filename = tar.filename;

            header_num = tar.header_num;
            header = tar.header;
        }

        //------------------------------------------------------------------------
        // Name             : CopyValue
        // Function         : 値をコピー
        // Argument			: コピー先のFileData
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void CopyValue(FileData tar)
        {
            val = tar.val;
        }

        //------------------------------------------------------------------------
        // Name             : InVal
        // Function         : 数値データを入力
        // Argument			: 入力する数値データ
        // Return			: -
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public void InVal(double[,] inval)
        {
            val = inval;
        }

        //------------------------------------------------------------------------
        // Name             : OutVal
        // Function         : 数値データを出力
        // Argument			: -
        // Return			: 数値データ
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public double[,] OutVal()
        {
            double[,] output = val;

            return output;
        }      
        

        //------------------------------------------------------------------------
        // Name             : OutSamples
        // Function         : 画像サイズ(X軸)を出力
        // Argument			: -
        // Return			: 数値データ
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public int OutSamples()
        {
            int output = samples;

            return output;
        }

        //------------------------------------------------------------------------
        // Name             : OutLines
        // Function         : 画像サイズ(Y軸)を出力
        // Argument			: -
        // Return			: 数値データ
        // Update			: 2018/03/17 K.Matsui
        // Refarences		: -
        // Comment			: -
        //------------------------------------------------------------------------
        public int OutLines()
        {
            int output = lines;

            return output;
        }

        #endregion

    }
}